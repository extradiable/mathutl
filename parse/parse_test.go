package parse

import "testing"

func TestParse(t *testing.T) {
	tcases := []struct {
		fun string
		prm []float64
		res float64
	}{
		{"0", []float64{}, 0},
		{"-0", []float64{}, 0},
		{"-10", []float64{}, -10},
		{"1+2+3+4", []float64{}, 10},
		{"-1-2-3-4", []float64{}, -10},
		{"1-2*3-3", []float64{}, -8},
		{"1+2*3+3", []float64{}, 10},
		{"(1+2)*3", []float64{}, 9},
		{"1+(2*3)", []float64{}, 7},
		{"4!", []float64{}, 24},
		{"-4!", []float64{}, -24},
		{"x", []float64{0}, 0},
		{"-x", []float64{0}, 0},
		{"-x", []float64{10}, -10},
		{"-x", []float64{-10}, 10},
		{"1+x+3+4", []float64{2}, 10},
		{"-1-x-3-4", []float64{2}, -10},
		{"1-x*3-3", []float64{2}, -8},
		{"1+x*3+3", []float64{2}, 10},
		{"(1+x)*3", []float64{2}, 9},
		{"1+(x*3)", []float64{2}, 7},
		{"x+(x*3)", []float64{2}, 8},
		{"x*(x^3+x^2+x)", []float64{2}, 28},
	}
	for _, c := range tcases {
		prog, err := Parse(c.fun)
		if err != nil {
			t.Errorf("%s", err.Error())
		} else {
			val, err := Eval(prog, c.prm...)
			if err != nil {
				t.Errorf("%s", err.Error())
			} else {
				if val != c.res {
					t.Errorf("\nParse(%q, %f) = %f != %f", c.fun, c.prm, val, c.res)
				}
			}
		}
	}
}
