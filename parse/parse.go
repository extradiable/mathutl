// Copyright 2019 Victor Adrian Valle Rivera. All rights reserved.
// Use of this source code is governed by a MIT License
// that can be found in the LICENSE file.

// Package parse provides functions to parse a mathematical expression into a MathExpr object.
// An Eval function is povided to evaluate the MathExpr object by providing concrete values to the
// independent variables used.
// Example:
// prog, _ := parse.Parse("(2*x^2)/6")
// val, err := parse.Eval(prog, 3)
package parse

import (
	"errors"
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"
)

var input string
var count int
var numbRegexp = regexp.MustCompile("^(0|[1-9][0-9]*\\.?[0-9]*)")
var varRegexp = regexp.MustCompile("^[a-z]")
var spaceRegexp = regexp.MustCompile("^\\s+")

type token struct {
	kind   int
	pos    int
	lexeme string
}

type param struct {
	name  string
	value float64
}

type MathExpr struct {
	operands  []float64
	variables []param
	varNames  map[string]int
	postfix   []int
}

var current token
var next token
var null token

var prog MathExpr

const (
	NOP = iota
	SIN
	COS
	TAN
	COT
	CSC
	SEC
	LOG
	LN
	PLS
	MIN
	TMS
	DVD
	FCT
	PWR
	LPN
	RPN
	EQS
	CMA
	NULL
	NUM
	ID
	UNK
)

var lexemes [CMA + 1]string

func init() {
	lexemes[NOP] = " "
	lexemes[SIN] = "sin"
	lexemes[COS] = "cos"
	lexemes[TAN] = "tan"
	lexemes[COT] = "cot"
	lexemes[CSC] = "csc"
	lexemes[SEC] = "sec"
	lexemes[LOG] = "log"
	lexemes[LN] = "ln"
	lexemes[PLS] = "+"
	lexemes[MIN] = "-"
	lexemes[TMS] = "*"
	lexemes[DVD] = "/"
	lexemes[FCT] = "!"
	lexemes[PWR] = "^"
	lexemes[LPN] = "("
	lexemes[RPN] = ")"
	lexemes[EQS] = "="
	lexemes[CMA] = ","

	null = token{kind: NULL, lexeme: ""}
}

func sumExp() error {
	if err := multExp(); err != nil {
		return err
	}
	for {
		if current.kind == PLS || current.kind == MIN {
			if current.kind == PLS {
				nextToken()
			}
			if err := multExp(); err != nil {
				return err
			}
			prog.postfix = append(prog.postfix, -PLS)
			continue
		}
		break
	}
	return nil
}

func multExp() error {
	if e := powExp(); e != nil {
		return e
	}
	for {
		if current.kind == DVD || current.kind == TMS {
			operator := current.kind
			nextToken()
			if e := powExp(); e != nil {
				return e
			}
			prog.postfix = append(prog.postfix, -operator)
			continue
		} else if current.kind == FCT {
			nextToken()
			prog.postfix = append(prog.postfix, -FCT)
			continue
		}
		break
	}
	return nil
}

func powExp() error {
	if err := fnExp(); err != nil {
		return err
	}
	for current.kind == PWR {
		nextToken()
		if err := powExp(); err != nil {
			return err
		}
		prog.postfix = append(prog.postfix, -PWR)
	}
	return nil
}

func fnExp() error {
	switch current.kind {
	case SIN, COS, TAN, SEC, CSC, COT, LOG, LN:
		operator := current.kind
		nextToken()
		if err := fnExp(); err != nil {
			return err
		}
		prog.postfix = append(prog.postfix, -operator)
	default:
		return operand()
	}
	return nil
}

func operand() error {
	if current.kind == NUM {
		value, err := strconv.ParseFloat(current.lexeme, 64)
		if err == nil {
			addOperands(value)
		} else {
			return buildError("unable to parse value")
		}
	} else if current.kind == ID {
		index, ok := prog.varNames[current.lexeme]
		if !ok {
			v := param{
				name:  current.lexeme,
				value: 0,
			}
			index = len(prog.variables)
			prog.varNames[current.lexeme] = index
			prog.variables = append(prog.variables, v)
		}
		prog.postfix = append(prog.postfix, index)
		prog.postfix = append(prog.postfix, -ID)
	} else if current.kind == LPN {
		nextToken()
		sumExp()
		if current.kind != RPN {
			return buildError("Parentesis expected")
		}
	} else if current.kind == MIN {
		isNeg := true
		for next.kind == MIN {
			nextToken()
			isNeg = !isNeg
		}
		nextToken()
		if isNeg {
			addOperands(-1)
			multExp()
			//operand()
			prog.postfix = append(prog.postfix, -TMS)
		} else {
			operand()
		}
		return nil
	} else {
		return buildError("Operand was expected")
	}
	nextToken()
	return nil
}

func buildError(msg string) error {
	out := fmt.Sprintf("%s:\n", msg)
	out = out + fmt.Sprintf("%s\n", input)
	pos := current.pos
	if current.kind == NULL {
		out = out + fmt.Sprintf("%s^\n", strings.Repeat(" ", len(input)))
	}
	out = out + fmt.Sprintf("%s^\n", strings.Repeat(" ", pos))
	return errors.New(out)
}

func addOperands(operators ...float64) {
	for _, val := range operators {
		prog.postfix = append(prog.postfix, len(prog.operands))
		prog.operands = append(prog.operands, val)
	}
}

func nextToken() {
	if next == null {
		consumeToken()
	}
	current = next
	consumeToken()
}

func consumeToken() {
	next = token{}
	match := spaceRegexp.FindStringIndex(input[count:])
	if match != nil {
		count = count + match[1]
	}
	if len(input[count:]) == 0 {
		next = null
		return
	}
	next.kind, next.lexeme = UNK, input[count:]
	for i, val := range lexemes {
		if strings.HasPrefix(input[count:], val) {
			next.lexeme = val
			next.pos = count
			count = count + len(val)
			next.kind = i
			return
		}
	}
	res := []*regexp.Regexp{varRegexp, numbRegexp}
	ids := []int{ID, NUM}
	for i, re := range res {
		match := re.FindStringIndex(input[count:])
		if match != nil {
			next.kind = ids[i]
			next.pos = count
			next.lexeme = input[count : count+match[1]]
			count = count + match[1]
			break
		}
	}
}

func ToString(prog MathExpr) string {
	output := ""
	for i := len(prog.postfix) - 1; i >= 0; i = i - 1 {
		val := prog.postfix[i]
		if val < 0 {
			if val == -ID {
				output = "ID, " + output
				if i > 0 {
					i = i - 1
					output = prog.variables[prog.postfix[i]].name + ", " + output
				}
			} else {
				output = lexemes[-val] + ", " + output
			}
		} else {
			output = fmt.Sprint(prog.operands[val]) + ", " + output
		}
	}
	return output
}

func Parse(fn string) (MathExpr, error) {
	next = null
	current = null
	prog = MathExpr{}
	prog.varNames = make(map[string]int)
	input = fn
	count = 0
	nextToken()
	if err := sumExp(); err != nil {
		return prog, err
	}
	if current.kind != NULL {
		return prog, buildError("End of expression expected")
	}
	return prog, nil
}

func Eval(me MathExpr, params ...float64) (float64, error) {
	var queue []int
	for _, v := range me.postfix {
		if v >= 0 {
			queue = append(queue, v)
		} else {
			v = -v
			if v == ID {
				pos := queue[len(queue)-1]
				queue[len(queue)-1] = len(me.operands)
				me.operands = append(me.operands, params[pos])
			} else if v == FCT {
				pos := queue[len(queue)-1]
				queue[len(queue)-1] = len(me.operands)
				val := 1
				base := int(me.operands[pos])
				if base < 0 {
					return 0, errors.New("Factorial of negative number")
				}
				for ; base > 0; base-- {
					val = val * base
				}
				me.operands = append(me.operands, float64(val))
			} else {
				length := len(queue)
				op1 := me.operands[queue[length-1]]
				op2 := me.operands[queue[length-2]]
				queue[length-2] = len(me.operands)
				queue = queue[0 : length-1]
				if v == PLS {
					me.operands = append(me.operands, op1+op2)
				} else if v == TMS {
					me.operands = append(me.operands, op1*op2)
				} else if v == DVD {
					if op1 == 0 {
						return 0, errors.New("Divide by zero")
					}
					me.operands = append(me.operands, op2/op1)
				} else if v == PWR {
					me.operands = append(me.operands, math.Pow(op2, op1))
				}
			}
		}
	}
	return me.operands[queue[len(queue)-1]], nil
}
